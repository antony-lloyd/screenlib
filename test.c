#include <stdio.h>
#include "screen_ui.h"

int main()
{
	struct screenWndSize;

	screenSetupScreen();

	UIMenuBar *menu = uiMenuBar_new("test");

	UIMenuList *file = uiMenuList_new("file");

	uiMenuBar_addList(menu, file);
	uiMenuBar_addList(menu, file);

	uiMenuBar_draw(menu);
}