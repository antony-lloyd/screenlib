#include "screen.h"

bool screenSetup; // Check to see if the screen has been setup, primaraly for Windows as Linux/Mac doesn't need to be set up

struct _WndSize
{
	int w, h;
};

struct _ColourInfo
{
	int bright_foreground, bright_background, foreground, background;
};

void screenSetupScreen()
{
	#ifdef TARGET_WINDOWS
	stdOutHdl = GetStdHandle(STD_OUTPUT_HANDLE);
	#endif

	screenSetup = true;
}

int getWndSizeWidth(screenWndSize *wndSize)
{
    if(wndSize == NULL)
        return -1;
    return wndSize->w;
}

int getWndSizeHeight(screenWndSize *wndSize)
{
    if(wndSize == NULL)
        return -1;
    return wndSize->h;
}

int getColourInfoBrightForeground(screenColourInfo *info)
{
    if(info == NULL)
        return -1;
    return info->bright_foreground;
}

int getColourInfoBrightBackground(screenColourInfo *info)
{
    if(info == NULL)
        return -1;
    return info->bright_background;
}

int getColourInfoForeground(screenColourInfo *info)
{
    if(info == NULL)
        return -1;
    return info->foreground;
}

int getColourInfoBackground(screenColourInfo *info)
{
    if(info == NULL)
        return -1;
    return info->background;
}

screenWndSize *screenGetConsoleSize()
{
	screenWndSize *wndsize = (screenWndSize*)malloc(sizeof(screenWndSize));

	if(!screenSetup)
	{
		wndsize->w = wndsize->h = 0;
		return wndsize;
	}

	#ifdef TARGET_WINDOWS

		CONSOLE_SCREEN_BUFFER_INFO inf;
		if(GetConsoleScreenBufferInfo(stdOutHdl, &inf))
		{
			wndsize->w = inf.srWindow.Right - inf.srWindow.Left + 1;
			wndsize->h = inf.srWindow.Bottom - inf.srWindow.Top + 1;
		}
		else
		{
			wndsize->w = 0;
			wndsize->h = 0;
		}

		return wndsize;

	#elif defined TARGET_LINUX

		struct winsize size;
		ioctl(0, TIOCGWINSZ, (char *) &size);

		wndsize->w = size.ws_col;
		wndsize->h = size.ws_row;

		return wndsize;

	#endif
}

bool screenSetCursorPosition(short x, short y)
{
	if(!screenSetup) return false;

	#ifdef TARGET_WINDOWS

	COORD CursorPosition = { x, y };

	SetConsoleCursorPosition(stdOutHdl, CursorPosition);

	#elif defined TARGET_LINUX
	printf("%c%c%i;%if", ESC1, ESC2, y + 1, x + 1);
	#endif

	return true;
}

bool screenSetConsoleColour(int bright_foreground, int bright_background, int foreground, int background)
{
	if(!screenSetup) return false;

	#ifdef TARGET_WINDOWS

	SetConsoleTextAttribute(stdOutHdl, bright_foreground | foreground | bright_background | background);

	#elif defined TARGET_LINUX

	printf("%c%c%i;%i;%i;%im", ESC1, ESC2, bright_foreground, bright_background, foreground, background);

	#endif

	return true;
}

bool screenSetConsoleColourFromInfo(screenColourInfo *info)
{
	return screenSetConsoleColour(info->bright_foreground, info->bright_background, info->foreground, info->background);
}

int screenGetWidth(screenWndSize *size)
{
	return size->w;
}

int screenGetHeight(screenWndSize *size)
{
	return size->h;
}

screenColourInfo *screenCreateColourInfo(int bright_foreground, int bright_background, int foreground, int background)
{
	screenColourInfo *info = (screenColourInfo*)malloc(sizeof(screenColourInfo));
	info->bright_foreground = bright_foreground;
	info->bright_background = bright_background;
	info->foreground = foreground;
	info->background = background;
	
	return info;
}