/****************************
 * Screenlib - v2
 *===========================
 * A C++ library for doing stuff with the terminal
 *===========================
 * Created by: Antony Lloyd
 * http://antony-lloyd.com
 *===========================
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Antony Lloyd http://antony-lloyd.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 ****************************/

#ifndef SCREEN_H
#define SCREEN_H

#define TARGET_LINUX
#define SCREEN_MAJOR_VERSION 2
#define SCREEN_MINOR_VERSION 0

#include <iostream>

#ifdef TARGET_WINDOWS
	#include <windows.h>
	
	#define sleep(x)				Sleep(x * 1000)

	#define COLOUR_NORMAL			0
	#define COLOUR_FOR_BRIGHT		FOREGROUND_INTENSITY
	#define COLOUR_BACK_BRIGHT		BACKGROUND_INTENSITY

	#define COLOUR_FOR_BLACK		0
	#define COLOUR_FOR_RED			FOREGROUND_RED
	#define COLOUR_FOR_GREEN		FOREGROUND_GREEN
	#define COLOUR_FOR_YELLOW		COLOUR_FOR_RED + COLOUR_FOR_GREEN
	#define COLOUR_FOR_BLUE			FOREGROUND_BLUE
	#define COLOUR_FOR_MAGENTA		COLOUR_FOR_RED + COLOUR_FOR_BLUE
	#define COLOUR_FOR_CYAN			COLOUR_FOR_GREEN + COLOUR_FOR_BLUE
	#define COLOUR_FOR_GRAY			COLOUR_FOR_RED + COLOUR_FOR_GREEN + COLOUR_FOR_BLUE

	#define COLOUR_BACK_BLACK		0
	#define COLOUR_BACK_RED			BACKGROUND_RED
	#define COLOUR_BACK_GREEN		BACKGROUND_GREEN
	#define COLOUR_BACK_YELLOW		COLOUR_BACK_RED + COLOUR_BACK_GREEN
	#define COLOUR_BACK_BLUE		BACKGROUND_BLUE
	#define COLOUR_BACK_MAGENTA		BACKGROUND_MAGENTA
	#define COLOUR_BACK_CYAN		COLOUR_BACK_GREEN + COLOUR_BACK_BLUE
	#define COLOUR_BACK_GRAY		COLOUR_BACK_RED + COLOUR_BACK_GREEN + COLOUR_BACK_BLUE

	HANDLE 							stdOutHdl;

#elif defined TARGET_LINUX

	#include <unistd.h>
	#include <sys/ioctl.h>
	#include <termios.h>

	#define ESC1					0x1b
	#define ESC2					0x5b

	#define COLOUR_NORMAL			0
	#define COLOUR_FOR_BRIGHT		1
	#define COLOUR_BACK_BRIGHT		5

	#define COLOUR_FOR_BLACK 		30
	#define COLOUR_FOR_RED 			31
	#define COLOUR_FOR_GREEN 		32
	#define COLOUR_FOR_YELLOW 		33
	#define COLOUR_FOR_BLUE 		34
	#define COLOUR_FOR_MAGENTA 		34
	#define COLOUR_FOR_CYAN 		36
	#define COLOUR_FOR_GRAY 		37

	#define COLOUR_BACK_BLACK 		40
	#define COLOUR_BACK_RED 		41
	#define COLOUR_BACK_GREEN 		42
	#define COLOUR_BACK_YELLOW 		43
	#define COLOUR_BACK_BLUE 		44
	#define COLOUR_BACK_MAGENTA 	44
	#define COLOUR_BACK_CYAN 		46
	#define COLOUR_BACK_GRAY 		47

#endif

namespace ScreenLib
{
	struct WndSize
	{
		short w, h;
	};
	
	struct ColourInfo
	{
		int bright_foreground, bright_background, foreground, background;
	};
	
	class Screen
	{
	public:
		static void 				setup();
		static void 				clear();
		static void 				setCursorPosition(short x, short y);
		static void 				setColour(int bright_foreground, int bright_background, int forground, int background);
		static void 				setColourFromInfo(ColourInfo info);
		
		static WndSize 				getSize();
		
	private:
		static bool 				hasSetup;
	};
}

#endif
