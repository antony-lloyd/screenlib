#include "screen.h"

using namespace std;
using namespace ScreenLib;

bool Screen::hasSetup = false;

/**
 * Setups the screen
 */
void Screen::setup()
{
	#ifdef TARGET_WINDOWS
	stdOutHdl = GetStdHandle(STD_OUTPUT_HANDLE);
	#endif

	Screen::hasSetup = true;
}

/**
 * Clears the screen
 */
void Screen::clear()
{
#ifdef TARGET_WINDOWS
	system("cls");
#elif defined TARGET_LINUX
	system("clear");
#endif
}

/**
 * Gets the screen size
 * @return WndSize The screen size
 */
WndSize Screen::getSize()
{
	WndSize size;

	if(!Screen::hasSetup) Screen::setup();

	#ifdef TARGET_WINDOWS

		CONSOLE_SCREEN_BUFFER_INFO inf;
		if(GetConsoleScreenBufferInfo(stdOutHdl, &inf))
		{
			size.w = inf.srWindow.Right - inf.srWindow.Left + 1;
			size.h = inf.srWindow.Bottom - inf.srWindow.Top + 1;
		}
		else
		{
			size.w = 0;
			size.h = 0;
		}

		return wndsize;

	#elif defined TARGET_LINUX

		struct winsize tsize;
		ioctl(0, TIOCGWINSZ, (char *) &tsize);

		size.w = tsize.ws_col;
		size.h = tsize.ws_row;

		return size;

	#endif
}

/**
 * Sets the cursor position
 * @param short x The X position
 * @param short y The Y position
 */
void Screen::setCursorPosition(short x, short y)
{
	if(!Screen::hasSetup) Screen::setup();

	#ifdef TARGET_WINDOWS

		COORD CursorPosition = { x, y };

		SetConsoleCursorPosition(stdOutHdl, CursorPosition);

	#elif defined TARGET_LINUX
		printf("%c%c%i;%if", ESC1, ESC2, y + 1, x + 1);
	#endif
}

/**
 * Sets the screen colour
 * @param int bright_foreground 1 If bright foreground colour
 * @param int bright_background 1 If bright background colour
 * @param int foreground The foreground colour
 * @param int background The background colour
 */
void Screen::setColour(int bright_foreground, int bright_background, int foreground, int background)
{
	if(!Screen::hasSetup) Screen::setup();

	#ifdef TARGET_WINDOWS

		SetConsoleTextAttribute(stdOutHdl, bright_foreground | foreground | bright_background | background);

	#elif defined TARGET_LINUX

		printf("%c%c%i;%i;%i;%im", ESC1, ESC2, bright_foreground, bright_background, foreground, background);

	#endif
}

/**
 * Sets the screen colour from a ColourInfo
 * @param ColourInfo info The colour info
 */
void Screen::setColourFromInfo(ColourInfo info)
{
	Screen::setColour(info.bright_foreground, info.bright_background, info.foreground, info.background);
}
